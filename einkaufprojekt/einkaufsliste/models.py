import datetime
from django.db import models

# Create your models here.

class Item(models.Model):
    name = models.TextField()
    menge = models.CharField(max_length=200, null=True, blank=True)
    status = models.BooleanField(default=False)
    preis = models.IntegerField(null=True, blank=True, default=0.0)
    liste = models.ForeignKey(to="Liste", on_delete=models.CASCADE)
    
    def __str__(self):
        return self.name


class Liste(models.Model):
    name = models.CharField(max_length=200)

    def erledigt(self):
        return Item.objects.filter(liste=self, status=True).count()


    def anzahl(self):
        return Item.objects.filter(liste=self).count()

    def __str__(self):
        return self.name