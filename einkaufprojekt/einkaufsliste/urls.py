from django.urls import path

from . import views

app_name = ''
urlpatterns = [
    path('', views.index, name='index'),
    path('liste/<int:liste_id>', views.liste, name='liste'),
    path('liste_anlegen', views.liste_anlegen, name='liste_anlegen')
]