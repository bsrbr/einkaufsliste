from django.http import Http404
from django.shortcuts import get_object_or_404, render, redirect

from .models import *

# Create your views here.

from django.http import HttpResponse
from django.template import loader


def index(request):
    listen = Liste.objects.all()
    items = Item.objects.all()
    context = {"listen": listen, "items": items}
    return render(request, "einkaufsliste/index.html", context)

def liste(request, liste_id):
    context = {}
    context["liste"] = get_object_or_404(models.Liste, pk=liste_id)
    context["eintraege"] = models.Item.objects.filter(liste=liste_id)
    summe = 0
    for eintrag in context["eintraege"]:
        summe += eintrag.preis * eintrag.menge
    context["summe"] = summe

    return render(request, "einkaufsliste/list.html", context)


def liste_anlegen(request):
    models.Liste.objects.create(name=request.POST["liste_name"])
    return redirect("index")

